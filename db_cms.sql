-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2016 at 01:30 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_05_11_205235_setup_anvard', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE `tb_banner` (
  `id` int(11) NOT NULL,
  `banner_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `banner_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `banner_all_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `banner_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_view` int(15) NOT NULL,
  `banner_show` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_banner`
--

INSERT INTO `tb_banner` (`id`, `banner_name`, `banner_file`, `banner_detail`, `banner_all_detail`, `banner_url`, `banner_view`, `banner_show`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'ทดสอบภาพ', 'cGPYgSBU1BNYBNkK', '', '', 'ทดสอบภาพ', 0, 1, '2016-10-30 16:44:49', '2016-10-30 16:44:49', 1),
(2, 'ทดสอบระบบข่าวประชาสัมพันธ์ โดยใช้ภาพ สไลด์', 'YrHxKPgo8pJOQsuF', 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla', '', 'ทดสอบระบบข่าวประชาสัมพันธ์-โดยใช้ภาพ-สไลด์', 0, 1, '2016-10-30 23:37:07', '2016-10-30 23:37:07', 1),
(3, 'ทดสอบระบบภาพสไลด์', 'FQN5UlVrNZb24GXf', '', '', 'ทดสอบระบบภาพสไลด์', 0, 1, '2016-10-30 23:38:12', '2016-10-30 23:38:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE `tb_categories` (
  `id` int(25) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `categories_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_show` int(2) NOT NULL,
  `categories_showhome` int(2) NOT NULL,
  `sort` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `parent_id`, `categories_name`, `categories_url`, `categories_show`, `categories_showhome`, `sort`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 0, 'ข่าวประชาสัมพันธ์', 'ข่าวประชาสัมพันธ์', 1, 1, 1, '2016-10-30 23:05:21', '2016-10-30 23:05:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_content`
--

CREATE TABLE `tb_content` (
  `id` int(11) NOT NULL,
  `content_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_categories` int(6) NOT NULL,
  `content_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_alldetail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_picture` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_view` int(15) NOT NULL,
  `content_showhome` int(1) NOT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_show` int(1) NOT NULL,
  `content_author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_year` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL,
  `parent_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_content`
--

INSERT INTO `tb_content` (`id`, `content_name`, `content_categories`, `content_detail`, `content_alldetail`, `content_file`, `content_picture`, `content_view`, `content_showhome`, `content_url`, `content_show`, `content_author`, `content_year`, `created_at`, `updated_at`, `created_by`, `parent_id`) VALUES
(1, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'WHXbpxPz8W17E79Z', '5SxQLyujF6e65wxU.jpg', 8, 1, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:06:44', '2016-11-02 14:47:34', 1, 1),
(2, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'UM5C7kP3z2gAXBwh', 'wnvaOMieOlld44KU.jpg', 0, 1, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:09:40', '2016-10-30 23:09:40', 1, 1),
(3, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'iuI0GwDEY0Wkc1Rr', 'GgYRCNeE4lwWkKEk.jpg', 0, 0, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:10:36', '2016-10-30 23:10:36', 1, 1),
(4, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'cHTBOPVP2hq4GScI', '62KFYRPZQzy05d1R.jpg', 0, 0, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:11:54', '2016-10-30 23:11:54', 1, 1),
(5, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'trcKD7U9UTnU9RHs', 'xgz3IAVvtpMI3dHu.jpg', 0, 0, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:13:57', '2016-10-30 23:13:57', 1, 1),
(6, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '<p>bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>\r\n', '', 'mPzEyBKkg3zVFfre', 'aO66UxrjTdVK8cfT.jpg', 0, 0, 'ทดสอบระบบข่าวประชาสัมพันธ์', 1, '', '', '2016-10-30 23:14:29', '2016-10-30 23:14:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_enviroment`
--

CREATE TABLE `tb_enviroment` (
  `id` int(11) NOT NULL,
  `web_name_lo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web_address` text COLLATE utf8_unicode_ci NOT NULL,
  `web_tel` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `web_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `web_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_enviroment`
--

INSERT INTO `tb_enviroment` (`id`, `web_name_lo`, `web_name_en`, `web_address`, `web_tel`, `web_email`, `web_keyword`, `web_detail`, `created_by`) VALUES
(3, 'ห้างหุ้นส่วนจำกัด ป.แจ่มสุวรรณทรัพย์', 'pjamsuwansup limited partnership', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_files`
--

CREATE TABLE `tb_files` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files`
--

INSERT INTO `tb_files` (`id`, `files_newname`, `files_oldname`, `files_type`, `files_size`, `token`) VALUES
(1, '', '', '0', 0, 'WHXbpxPz8W17E79Z'),
(2, '', '', '0', 0, 'UM5C7kP3z2gAXBwh'),
(3, '', '', '0', 0, 'iuI0GwDEY0Wkc1Rr'),
(4, '', '', '0', 0, 'cHTBOPVP2hq4GScI'),
(5, '', '', '0', 0, 'trcKD7U9UTnU9RHs'),
(6, '', '', '0', 0, 'mPzEyBKkg3zVFfre');

-- --------------------------------------------------------

--
-- Table structure for table `tb_files_banner`
--

CREATE TABLE `tb_files_banner` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files_banner`
--

INSERT INTO `tb_files_banner` (`id`, `files_newname`, `files_oldname`, `files_thumb`, `files_type`, `files_size`, `token`) VALUES
(1, 'jVxzlpJakkx8UP3Q.jpg', '1791833.jpg', 'jVxzlpJakkx8UP3Q.jpg', 'jpg', 130019, 'cGPYgSBU1BNYBNkK'),
(2, 'DnRKfLZ2DMoOX2e0.jpg', '1791846.jpg', 'DnRKfLZ2DMoOX2e0.jpg', 'jpg', 167988, 'YrHxKPgo8pJOQsuF'),
(3, 'd7cGlIykQkQGmg5W.jpg', '1791840.jpg', 'd7cGlIykQkQGmg5W.jpg', 'jpg', 160740, 'FQN5UlVrNZb24GXf');

-- --------------------------------------------------------

--
-- Table structure for table `tb_files_gallery`
--

CREATE TABLE `tb_files_gallery` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files_gallery`
--

INSERT INTO `tb_files_gallery` (`id`, `files_newname`, `files_oldname`, `files_thumb`, `files_type`, `files_size`, `token`) VALUES
(1, 'UcQqs4xWrqf6iunK.jpg', '1791845.jpg', 'UcQqs4xWrqf6iunK.jpg', 'jpg', 133336, 'dj0mATH9l6fYQw09'),
(2, 'HWw6ZXbOfZKpmzOg.jpg', '1791847.jpg', 'HWw6ZXbOfZKpmzOg.jpg', 'jpg', 190798, 'dj0mATH9l6fYQw09'),
(3, '4XP7IO0iaQSMwVlI.jpg', '1791833.jpg', '4XP7IO0iaQSMwVlI.jpg', 'jpg', 130019, 'JJfi3e0NqTCiZoxC'),
(4, 'OS49hipXLsRmn7pf.jpg', '1791832.jpg', 'OS49hipXLsRmn7pf.jpg', 'jpg', 111026, 'JJfi3e0NqTCiZoxC'),
(5, '4E6ljIy2LGbnocp9.jpg', '1791833.jpg', '4E6ljIy2LGbnocp9.jpg', 'jpg', 130019, 'JJfi3e0NqTCiZoxC'),
(6, '3f7KtLUm0DzkoUN3.jpg', '1791834.jpg', '3f7KtLUm0DzkoUN3.jpg', 'jpg', 222972, 'ukwFnn2Zq55WQaSS'),
(7, 'Cm9PG52ihhx6vXUa.jpg', '1791844.jpg', 'Cm9PG52ihhx6vXUa.jpg', 'jpg', 119023, 'oKjY2dnKGCQm8yMJ');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gallery`
--

CREATE TABLE `tb_gallery` (
  `id` int(11) NOT NULL,
  `gallery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `gallery_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_view` int(15) NOT NULL,
  `gallery_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_sorting` int(11) NOT NULL,
  `gallery_show` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL,
  `parent_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_gallery`
--

INSERT INTO `tb_gallery` (`id`, `gallery_name`, `gallery_detail`, `gallery_file`, `gallery_view`, `gallery_url`, `gallery_sorting`, `gallery_show`, `created_at`, `updated_at`, `created_by`, `parent_id`) VALUES
(1, 'ทดสอบภาพผลงานล่าสุด', '', 'dj0mATH9l6fYQw09', 0, 'ทดสอบภาพผลงานล่าสุด', 0, 1, '2016-10-30 23:15:58', '2016-10-30 23:15:58', 1, 0),
(2, 'ทดสอบภาพผลงานล่าสุดครั้งที่ 2', '<p>พนักงานประชุมเกี่ยวกับงาน</p>\r\n', 'JJfi3e0NqTCiZoxC', 0, 'ทดสอบภาพผลงานล่าสุดครั้งที่-2', 0, 1, '2016-10-30 23:17:12', '2016-10-30 23:17:12', 1, 0),
(3, 'กำลังขุดๆๆ', '', 'ukwFnn2Zq55WQaSS', 0, 'กำลังขุดๆๆ', 0, 1, '2016-10-30 23:18:59', '2016-10-30 23:18:59', 1, 0),
(4, 'ช่างกำลังเชื่อมโครงสร้าง', '', 'oKjY2dnKGCQm8yMJ', 0, 'ช่างกำลังเชื่อมโครงสร้าง', 0, 1, '2016-10-30 23:19:22', '2016-10-30 23:19:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mainmenu`
--

CREATE TABLE `tb_mainmenu` (
  `id` int(25) NOT NULL,
  `mainmenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_type` int(6) NOT NULL,
  `mainmenu_detail` longtext COLLATE utf8_unicode_ci,
  `mainmenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mainmenu_embed` longtext COLLATE utf8_unicode_ci,
  `m_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_sorting` int(6) NOT NULL,
  `mainmenu_position` int(2) DEFAULT NULL,
  `mainmenu_show` int(1) DEFAULT NULL,
  `mainmenu_showhome` int(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_mainmenu`
--

INSERT INTO `tb_mainmenu` (`id`, `mainmenu_name`, `mainmenu_type`, `mainmenu_detail`, `mainmenu_url`, `mainmenu_embed`, `m_url`, `mainmenu_sorting`, `mainmenu_position`, `mainmenu_show`, `mainmenu_showhome`, `parent_id`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'เกี่ยวกับ ป.แจ่มสุวรรณทรัพย์', 1, '<p><span style="font-size:24px"><span style="font-size:28px"><span style="color:rgb(2, 135, 73)">หจก.ป.แจ่มสุวรรณทรัพย์</span></span> ได้จดทะเบียนเป็นนิติบุคคล เมื่อวันที่ 25 ตุลาคม 2548 </span></p>\r\n\r\n<p><span style="font-size:24px">สำนักงานใหญ่ตั้งอยู่ที่ เลขที่ 218/13 หมู่ที่ 2 ตำบลนอกเมือง อำเภอเมือง จังหวัดสุรินทร์</span></p>\r\n\r\n<p><span style="font-size:24px">มีประสบการณ์และเชี่ยวชาญในงานมากกว่า 10 ปี&nbsp; โดยทางเราให้บริการต่างๆ ดังนี้</span></p>\r\n\r\n<ul>\r\n	<li><span style="font-size:24px">บริการรับเหมาก่อสร้าง</span></li>\r\n	<li><span style="font-size:24px">งานโครงสร้างเหล็กรูปพรรณ</span></li>\r\n	<li><span style="font-size:24px">งานระบบ Pipeping</span></li>\r\n	<li><span style="font-size:24px">งานติดตั้งระบบเครื่องจักร</span></li>\r\n	<li><span style="font-size:24px">งานติดตั้งระบบ Water Plant</span></li>\r\n	<li><span style="font-size:24px">งานติดตั้ง Boiler</span></li>\r\n	<li><span style="font-size:24px">ระบบงานก่อสร้างอื่นๆ</span></li>\r\n</ul>\r\n\r\n<p><span style="font-size:24px">โดยทีมงานที่มีประสบการณ์ ความชำนาญ มีมาตรฐานเป็นที่ยอมรับของลูกค้าหจก.ป.แจ่มสุวรรณทรัพย์</span></p>\r\n\r\n<p><span style="font-size:24px">ได้ให้ความสำคัญกับการบริการ มีความรับผิดชอบต่องานเป็นหลักสำคัญ ให้ความมั่นใจได้ว่าได้งานคุณภาพและได้รับความพึ่งพอใจ</span></p>\r\n\r\n<p><span style="font-size:24px">อย่างสูงสุดแน่นอน</span></p>\r\n', 'http://', '', 'เกี่ยวกับ-ป.แจ่มสุวรรณทรัพย์', 1, 1, 1, NULL, 0, '2016-10-30 16:25:47', '2016-11-02 14:38:37', 1),
(2, 'บริการของเรา', 3, '', 'http://', '', 'บริการของเรา', 2, 1, 1, NULL, 0, '2016-10-30 22:45:06', '2016-11-02 11:08:36', 1),
(3, 'ผลงานของเรา', 2, '', 'http://localhost/pjam/public/allgallery', '', 'ผลงานของเรา', 3, 1, 1, NULL, 3, '2016-10-30 22:45:26', '2016-11-02 15:41:24', 1),
(4, 'ติดต่อเรา', 1, '<p><span style="font-size:26px"><strong>ที่ตั้งสำนักงาน</strong></span></p>\r\n\r\n<p><span style="font-size:24px"><span style="color:#028749">ห้างหุ้นส่วนจำกัด ป.แจ่มสุวรรณทรัพย์</span> 218/13 ม.2 ต.นอกเมือง อ.เมือง จ.สุรินทร์ 32000</span></p>\r\n\r\n<p><span style="font-size:24px">เบอร์โทร 044-040-539 แฟกซ์ 044-530-067</span></p>\r\n\r\n<p><iframe frameborder="0" height="600" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2574.7274345786773!2d103.52344769981941!3d14.876970773817499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x63664e1cf6afac3e!2z4Lir4Lih4Li54LmI4Lia4LmJ4Liy4LiZ4Liq4Li04LiZ4LiX4Lij!5e0!3m2!1sth!2sth!4v1478066008923" style="border:0" width="800"></iframe></p>\r\n', 'http://', '', 'ติดต่อเรา', 4, 1, 1, NULL, 0, '2016-10-30 22:46:06', '2016-11-02 14:02:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_permission`
--

CREATE TABLE `tb_permission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `p_permission` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_permission`
--

INSERT INTO `tb_permission` (`id`, `user_id`, `token`, `p_permission`) VALUES
(2, 1, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_prefix`
--

CREATE TABLE `tb_prefix` (
  `id` int(25) NOT NULL,
  `prefix_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prefix_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_prefix`
--

INSERT INTO `tb_prefix` (`id`, `prefix_name`, `prefix_url`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'นาย', 'นาย', '2016-08-13 21:16:43', '2016-08-13 21:16:43', 1),
(2, 'นาง', 'นาง', '2016-08-13 21:16:52', '2016-08-13 21:16:52', 1),
(3, 'นางสาว', 'นางสาว', '2016-08-13 21:16:59', '2016-08-13 21:18:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_profiles`
--

CREATE TABLE `tb_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `webSiteURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profileURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthMonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthYear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailVerified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverInfoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_profiles`
--

INSERT INTO `tb_profiles` (`id`, `user_id`, `provider`, `identifier`, `webSiteURL`, `profileURL`, `photoURL`, `displayName`, `description`, `firstName`, `lastName`, `gender`, `language`, `age`, `birthDay`, `birthMonth`, `birthYear`, `email`, `emailVerified`, `phone`, `address`, `country`, `region`, `city`, `zip`, `username`, `coverInfoURL`, `created_at`, `updated_at`, `token`) VALUES
(2, 1, '', '', NULL, NULL, 'NVUrox8gjYvQfpNo.jpg', NULL, NULL, 'sontaya', 'upatum', NULL, NULL, NULL, NULL, NULL, NULL, 'kscomsci@gmail.com', NULL, '0813209321', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-30 17:00:00', '2016-08-30 17:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_submenu`
--

CREATE TABLE `tb_submenu` (
  `id` int(25) NOT NULL,
  `submenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_categories` int(6) NOT NULL,
  `submenu_type` int(6) NOT NULL,
  `submenu_detail` longtext COLLATE utf8_unicode_ci,
  `submenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_show` int(1) NOT NULL,
  `submenu_sorting` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_submenu`
--

INSERT INTO `tb_submenu` (`id`, `submenu_name`, `submenu_categories`, `submenu_type`, `submenu_detail`, `submenu_url`, `s_url`, `submenu_show`, `submenu_sorting`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'งานโครงสร้างเหล็กรูปพรรณ', 2, 1, '', 'http://', 'งานโครงสร้างเหล็กรูปพรรณ', 1, 1, '2016-11-02 11:09:00', '2016-11-02 11:09:00', 1),
(2, 'ระบบสปริงเกอร์', 2, 1, '', 'http://', 'ระบบสปริงเกอร์', 1, 2, '2016-11-02 11:09:14', '2016-11-02 11:09:14', 1),
(3, 'ระบบงานก่อสร้างอื่นๆ', 2, 1, '', 'http://', 'ระบบงานก่อสร้างอื่นๆ', 1, 3, '2016-11-02 11:09:24', '2016-11-02 11:09:24', 1),
(4, 'ระบบงานติดตั้งเครื่องจักร', 2, 1, '', 'http://', 'ระบบงานติดตั้งเครื่องจักร', 1, 4, '2016-11-02 11:09:34', '2016-11-02 11:09:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tag`
--

CREATE TABLE `tb_tag` (
  `id` int(25) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_count` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tag`
--

INSERT INTO `tb_tag` (`id`, `tag_name`, `tag_url`, `tag_count`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'ข่าวประชาสัมพันธ์', 'ข่าวประชาสัมพันธ์', 0, '2016-08-10 20:33:48', '2016-10-31 00:31:09', 1),
(2, 'ข่าวกิจจกรรม', 'ข่าวกิจจกรรม', 0, '2016-08-11 17:46:35', '2016-08-11 17:46:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagcontent`
--

CREATE TABLE `tb_tagcontent` (
  `id` int(11) NOT NULL,
  `tag_id` int(6) NOT NULL,
  `content_id` int(15) NOT NULL,
  `tag_count` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tagcontent`
--

INSERT INTO `tb_tagcontent` (`id`, `tag_id`, `content_id`, `tag_count`) VALUES
(1, 1, 1, 0),
(2, 1, 1, 0),
(3, 1, 1, 0),
(4, 1, 1, 0),
(5, 1, 1, 0),
(6, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(25) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(3) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_code` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_status`, `user_type`, `user_code`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', '$2y$10$GuTff3VDfuBRQmJsjh09L.l0aA6HQCq6L.qQ0ZoRye98U0HCTd.cS', 1, 1, '', '2015-10-20 00:00:00', '2016-11-02 16:00:49', 'n4Mcx0bPYcEFfeuLONJz89SswbUHoJ7djftyJpcdHmqQNlb8IuVbpHkQUsOB');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banner`
--
ALTER TABLE `tb_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_content`
--
ALTER TABLE `tb_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files`
--
ALTER TABLE `tb_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files_banner`
--
ALTER TABLE `tb_files_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files_gallery`
--
ALTER TABLE `tb_files_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gallery`
--
ALTER TABLE `tb_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_permission`
--
ALTER TABLE `tb_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_prefix`
--
ALTER TABLE `tb_prefix`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profiles`
--
ALTER TABLE `tb_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tag`
--
ALTER TABLE `tb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banner`
--
ALTER TABLE `tb_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_content`
--
ALTER TABLE `tb_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_files`
--
ALTER TABLE `tb_files`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_files_banner`
--
ALTER TABLE `tb_files_banner`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_files_gallery`
--
ALTER TABLE `tb_files_gallery`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_gallery`
--
ALTER TABLE `tb_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_permission`
--
ALTER TABLE `tb_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_prefix`
--
ALTER TABLE `tb_prefix`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_profiles`
--
ALTER TABLE `tb_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_tag`
--
ALTER TABLE `tb_tag`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
