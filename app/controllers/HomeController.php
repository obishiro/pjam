<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 


	public function index()
	{
		$template =  Config::get('app.template');
		$Enviroment = Enviroment::first();
		$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->orderBy('sort','asc')->get();//all();
		$news = Content::select('tb_content.id','tb_content.content_name','tb_content.content_detail','tb_content.content_url','tb_content.content_picture','tb_content.created_at','tb_content.content_view')
          ->where(array(
           
            'tb_content.content_show'   =>'1'
            ))
          // ->join('tb_files','tb_files.token','=','tb_content.content_file')
          ->orderBy('tb_content.id','desc')->get();
          // $firstnews = Content::select('tb_content.id','tb_content.content_name','tb_content.content_detail','tb_content.content_url','tb_content.content_picture','tb_content.created_at','tb_content.content_view')
          // ->where(array(
           
          //   'tb_content.content_show'   =>'1'
          //   ))
          // // ->join('tb_files','tb_files.token','=','tb_content.content_file')
          // ->orderBy('tb_content.id','desc')->take(1)->skip(0)->get();
	 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
	 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
	 	->where('tb_banner.banner_show','1')
	 	->orderBy('tb_banner.id','desc')
	 	->get();
	 	$cBanner = Banner::select('tb_banner.banner_name')
	  
	 	->where('tb_banner.banner_show','1')->count();
	 	 
	 	$Gallery = Gallery::select('tb_gallery.gallery_name','tb_gallery.gallery_detail','tb_gallery.gallery_url','tb_files_gallery.files_newname')
		->join('tb_files_gallery','tb_files_gallery.token','=','tb_gallery.gallery_file')	 	
	 	->where(array('tb_gallery.gallery_show'=>'1'))
	 	->groupBy('tb_gallery.id')
	 	->orderBy('tb_gallery.gallery_sorting','asc')
	 	->get();
	 	$service = Submenu::where('submenu_categories','2')->orderBy('submenu_sorting','asc')->get();
		$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1'))->orderBy('mainmenu_sorting','asc')->get();
		$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
		$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
		
		return View::make('frontend.'.$template.'.index')->with(array(
			'env'		=> $Enviroment,
			'Categories'	=> $Categories,
			'Mainmenu_top'	=> $Mainmenu_top,
			'Mainmenu_right'	=> $Mainmenu_right,
			'Mainmenu_left'	=> $Mainmenu_left,
			'Banner'	=>$Banner,
			'i'			=>1,
			'Gallery'	=>$Gallery,
			'news'		=>$news,
			// 'firstnews' =>$firstnews
			'cBanner'	=>$cBanner,
			'service'	=>$service

			));
	 }

          
}
