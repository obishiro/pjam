@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$data->content_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
<div class="row">
<div class="col-md-9">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="{{ URL::to('/')}}">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					<li><a href="{{URL::to('allnews',array($data->categories_url))}}" target="_blank">{{ $data->categories_name }}</a></li>
					<li class="active">{{ $data->content_name}}</li>
				</ol>
			 
			 	<div class="blog-news">
			 	<div class="addthis_sharing_toolbox"></div>
			 		<h4 class="header-read-news"><i class="fa fa-bullhorn" aria-hidden="true"></i> {{ $data->content_name}}</h4>
			 		<div class="sub-read-news">
			 			<i class="fa fa-calendar" aria-hidden="true"></i>
 							เผยแพร่เมื่อ {{ Helpers::DateFormat($data->created_at) }}
 							&nbsp;&nbsp;
 						<i class="fa fa-eye" aria-hidden="true"></i>
 							อ่าน {{ number_format($data->content_view)}} ครั้ง
 							&nbsp;&nbsp;
 						<i class="fa fa-user" aria-hidden="true"></i>
 							โพสโดย ผู้ดูแลระบบ
					</div>

					
			 		{{ $data->content_detail}}
			 		{{ $data->content_alldetail}}
			 	</div>
			 		@if($Numfiles > 0)
					<div class="blog-download">
						<h4>
						<i class="fa fa-cloud-download" aria-hidden="true"></i>
						{{ Lang::get('msg.msg_download', array(), 'th')}}
						</h4>
						<ul class="list-inline">
							@foreach($Files as $Filesname => $f)
							<li>
								<i class="fa fa-caret-right" aria-hidden="true"></i>
									<a href="{{ URL::to('download',array($f->files_newname))}}" target="_blank">
 									{{ $f->files_oldname}}
 									</a>
							</li>
							@endforeach
						</ul>
					</div>
					@endif
			 
				
			<!-- Related Products Ends -->
			</div>
				<div class="col-md-3">
					@foreach($Datarandom as $dataran =>$dr)
					<div class="boxmenu">
						<div class="media">
							<div class="media-left">
						<a target="_blank" href="{{ URL::to('news',array($dr->content_url))}}">
       				<img class="img-responsive" style="margin-top:5px"  src="{{URL::to('uploadfiles/news/thumb',array($dr->content_picture))}}" >
       				</a>
       				</div>
       				<div >
       					<a target="_blank" href="{{ URL::to('news',array($dr->content_url))}}">
       						<h6>{{ $dr->content_name}}</h6>
       					</a>
       				</div>
					</div>
				</div>
					@endforeach
				</div>
		</div>
@stop
@section('script')
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
 