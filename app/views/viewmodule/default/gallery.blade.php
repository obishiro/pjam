@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$Title)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
<div class="row">
<div class="col-md-9">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="{{ URL::to('/')}}">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					 
					<li class="active">{{ $data->gallery_name}}</li>
				</ol>
			 
			 	 
  					<div class="col-xs-12 gallery" >
  						<h4 class="header-read-news"><i class="fa fa-bullhorn" aria-hidden="true"></i> {{ $data->gallery_name}}</h4>
			 		<div class="sub-read-news">
			 			<i class="fa fa-calendar" aria-hidden="true"></i>
 							เผยแพร่เมื่อ {{ Helpers::DateFormat($data->created_at) }}
 							&nbsp;&nbsp;
 						<i class="fa fa-eye" aria-hidden="true"></i>
 							อ่าน {{ number_format($data->gallery_view)}} ครั้ง
 							&nbsp;&nbsp;
 						<i class="fa fa-user" aria-hidden="true"></i>
 							โพสโดย ผู้ดูแลระบบ
					</div>
			 		{{ $data->gallery_detail}}
			 		<br>
			 	 		@foreach($Gallery_file as $Gallery =>$g)
						  <div class="col-xs-6 col-md-3">
						    <a href="{{ URL::to('uploadfiles/gallery',array($g->files_newname))}}"  rel="fancybox-thumb" class="thumbnail fancybox-thumb">
						      <img src="{{ URL::to('uploadfiles/gallery/thumb',array($g->files_newname))}}" >
						    </a>
						  </div>
						@endforeach
  					</div>

			 
				
			<!-- Related Products Ends -->
			</div>
				<div class="col-md-3">
					 
				</div>
		</div>
@stop
