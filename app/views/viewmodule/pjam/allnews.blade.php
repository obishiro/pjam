@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$Title)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
<div class="container">
<div class="row">
<div class="col-md-12">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="{{ URL::to('/')}}">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					 
					<li class="active">{{ $Title}}</li>
				</ol>
			 
			 	<div class="blog-news">
			 		    @foreach($data as $new => $n)
       		<div class="media news-content" >
       			<div class="col-xs-4" style="margin-left:-15px;"> 
       			<a  href="{{ URL::to('news',array($n->content_url))}}">
       			<img class="img-responsive img-thumbnail"  src="{{URL::to('uploadfiles/news/thumb',array($n->content_picture))}}">
       			</a>
       			</div>
       			<div class="col-xs-8">
       				<h4><a  href="{{ URL::to('news',array($n->content_url))}}">{{ $n->content_name}}</a></h4>
              <div class="sub-read-news">
       				<i class="fa fa-calendar" aria-hidden="true"></i>
              เผยแพร่เมื่อ {{ Helpers::DateFormat($n->created_at) }}
              &nbsp;&nbsp;
            <i class="fa fa-eye" aria-hidden="true"></i>
              อ่าน {{ number_format($n->content_view)}} ครั้ง
              &nbsp;&nbsp;
            <i class="fa fa-user" aria-hidden="true"></i>
              โพสโดย ผู้ดูแลระบบ
              </div>
       				{{$n->content_detail}}
					<a  href="{{ URL::to('news',array($n->content_url))}}">อ่านเพิ่มเติม</a>
       			</div>
       		</div>
		  @endforeach
		  <?php echo $data->links(); ?>
			 	</div>
		 
			 
				
			<!-- Related Products Ends -->
			</div>
				 
		</div>
	</div>
@stop
 
 