@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$data->banner_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
<div class="container">
<div class="row">
<div class="col-md-12">
			<!-- Breadcrumb Starts -->
				<ol class="breadcrumb">
					<li><a href="{{ URL::to('/')}}">{{ Lang::get('frontend.home',array(),'th') }}</a></li>
					 
					<li class="active">{{ $data->banner_name}}</li>
				</ol>
			 
			 	<div class="blog-news">
			 	<div class="addthis_sharing_toolbox"></div>
			 		<h4 class="header-read-news"><i class="fa fa-bullhorn" aria-hidden="true"></i> {{ $data->banner_name}}</h4>
			 		<div class="sub-read-news">
			 			<i class="fa fa-calendar" aria-hidden="true"></i>
 							เผยแพร่เมื่อ {{ Helpers::DateFormat($data->created_at) }}
 							&nbsp;&nbsp;
 						{{-- <i class="fa fa-eye" aria-hidden="true"></i>
 							อ่าน {{ number_format($data->banner_view)}} ครั้ง
 							&nbsp;&nbsp; --}}
 						<i class="fa fa-user" aria-hidden="true"></i>
 							โพสโดย ผู้ดูแลระบบ
					</div>
			 		{{ $data->banner_detail}}
			 		{{ $data->banner_all_detail}}
			 		 
			 	</div>

			 
				
			<!-- Related Products Ends -->
			</div>
				 
		</div>
	</div>
@stop
@section('script')
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
