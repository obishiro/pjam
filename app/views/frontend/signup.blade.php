 <link rel="stylesheet" href="{{ URL::to('css/bootstrap.css') }}">
 <link rel="stylesheet" href="{{ URL::to('css/bootstrap-social.css') }}">
  <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }} ">
 <div class="container-fluid row">
 <div class="col-sm-4">
 <h2>ลงทะเบียน <small>สมัครสมาชิก</small></h2>
			{{ Form::open(array(
				'url'=>'signup',
				'method'=>'post'

			))}}
<div class="row">
  <div class="col-xs-12  col-md-6">
  	 <div class="form-group">
  				{{ 
				Form::text('txt-firstname','',array(
				'class'=>'form-control',
				'placeholder' =>'ชื่อ',
				'data-validetta'=>'required'
			))
			}}
	</div>

  </div>
  <div class="col-xs-12 col-md-6">
  	<div class="form-group required">
			{{ 
				Form::text('txt-surname','',array(
				'class'=>'form-control',
				'placeholder' =>'นามสกุล',
				'data-validetta'=>'required'
			))
			}}
	</div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
  	<div class="form-group">
 
			{{ 
				Form::text('txt-email','',array(
				'class'=>'form-control',
				'placeholder' =>'Email',
				'data-validetta'=>'required,email'
			))
			}}
			</div>
  </div>
  </div>
  <div class="row">
  <div class=" col-md-12">
  	<div class="form-group">
 		<input type="password" class="form-control" placeholder="รหัสผ่าน" data-validetta="required">
 	</div>
  </div>
  </div>
    <div class="row">
  <div class=" col-md-12">
  	<div class="form-group">
 		 <select name="gender" id="" class="form-control"  data-validetta="required">
 		 	<option value="">เลือกเพศ</option>
 		 	<option value="1">ชาย</option>
 		 	<option value="2">หญิง</option>
 		 </select>
 	</div>
  </div>
  </div>
  <div class="row">
  		<div class="col-md-4">
  		<select name="" id="" class="form-control"   data-validetta="required">>
  				<option value="">วันเกิด</option>
  				<option value=""></option>
  				<option value=""></option>
  			</select>
  		</div>
  		<div class="col-md-4">
  		<select name="" id="" class="form-control"   data-validetta="required">>
  				<option value="">เดือนเกิด</option>
  				<option value=""></option>
  				<option value=""></option>
  			</select>
  		</div>
  		<div class="col-md-4">
  		<select name="" id="" class="form-control"  data-validetta="required">>
  				<option value="">ปีเกิด</option>
  				<option value=""></option>
  				<option value=""></option>
  			</select>
  		</div>
  </div>
  <br>
    <div class="row">
  		<div class="col-md-12">
			<div class="form-group">
			{{ 
				Form::submit(Lang::get('msg.msg_user_register',array(),'th'),array(
				'class'=>'btn btn-primary'
			))
			}}
			{{
				Form::reset(Lang::get('msg.msg_cancle',array(),'th'),array(
					'class'=>'btn btn-danger'
					))
				}}
			</div>



			{{ form::close()}}
		</div>
		</div>

			</div>
		 
 				<div class="col-sm-4">
		 		<h2>ลงทะเบียนผ่าน <small>Socail network</small></h2>
		 		 <a class="btn btn-block btn-social btn-facebook" href="{{ URL::to('auth/loginfacebook')}}">
    				<span class="fa fa-facebook"></span> Sign in with Facebook
  				</a>
		 		 <a class="btn btn-block btn-social btn-twitter">
    				<span class="fa fa-twitter"></span> Sign in with Twitter
  				</a>

  				<a class="btn btn-block btn-social btn-google">
    				<span class="fa fa-google"></span> Sign in with Google+
  				</a>
			 </div>
			 

	</div>
 
 