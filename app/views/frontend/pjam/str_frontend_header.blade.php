<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keyword" content="@yield('keyword')">
    <meta name="viewport" content="width=device-width">
    
    <!-- core CSS -->
    <link href="{{ URL::to('frontend/pjam/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/main.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/responsive.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/styles.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/slippry.css')}}" rel="stylesheet">
    <link href="{{ URL::to('frontend/pjam/css/jquery.bxslider.css')}}" rel="stylesheet">
     <link rel="stylesheet" href="{{ URL::to('css/jquery.fancybox.css') }}">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
   {{--  <link rel="shortcut icon" href="{{ URL::to('frontend/pjam/images/ico/favicon.ico')}}"> --}}
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('frontend/pjam/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('frontend/pjam/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('frontend/pjam/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::to('frontend/pjam/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->
<body class="homepage">
