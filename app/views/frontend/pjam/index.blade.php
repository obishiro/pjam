@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$env->web_name_lo)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

 <!-- Page Content -->
     <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                @for ($ib = 0; $ib < $cBanner; $ib++)
                <li data-target="#main-slider" data-slide-to="{{ $ib }}" @if($ib==0) class="active" @endif ></li>
                @endfor
            </ol>
            <div class="carousel-inner">
                @foreach($Banner as $banner => $b)
                <div class="item @if($i==1) active @endif" style="background-image: url({{ URL::to('uploadfiles/banner',$b->files_newname)}})">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">{{ $b->banner_name}}</h1>
                                    <h2 class="animation animated-item-2">{{ $b->banner_detail}}</h2>
                                    <a class="btn-slide animation animated-item-3" href="{{ URL::to('slide',array($b->banner_url))}}">อ่านเพิ่มเติม</a>
                                </div>
                            </div>

                           
                        </div>
                    </div>
                </div><!--/.item-->
                 <?php $i++;?>
                @endforeach

               
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->
    <section id="services" class="service-item">
       <div class="container">
            <div class="center wow fadeInDown">
                <h2 style="color: #028749">บริการของเรา</h2>
                <p class="lead">คำอธบายเกี่ยวกับบริการของเรา</p>
            </div>
<div class="row">
                <div class="features">
                    @foreach($service as $services =>$serv)
                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-bullhorn"></i>
                            <h2 style="color: #028749"><a href="{{ URL::to('submenu',$serv->s_url)}}" >{{ $serv->submenu_name}}</a></h2>
                            <h3>เกี่ยวกับ{{ $serv->submenu_name}}</h3>
                        </div>
                    </div> 
                    @endforeach
                
                    
                </div> 
            </div> 
                    </div>
                
    </section><!--/#services-->
 
<section id="feature" >

        <div class="container">
           <div class="center wow fadeInDown">
                <h2 style="color: #028749"><a class=" "   href="{{ URL::to('allnews',array('ข่าวประชาสัมพันธ์'))}}">ข่าวสารและกิจกรรมต่างๆ</a></h2>
                <p class="lead">ข่าวสารและกิจกรรมความเคลื่อนไหว  <br> ห้างหุ่นส่วนจำกัด ป.แจ่มสุวรรณทรัพย์</p>
            </div>
 
            <div class="row">
 
 <section id="news-demo">
 @foreach($news as $new => $n)
  <article>
    <div class="text-content">
      <h2> <a  href="{{ URL::to('news',array($n->content_url))}}">{{ $n->content_name}}</a></h2>
      <p>{{$n->content_detail}}</p>
    
    </div>
    <div class="image-content"><img src="{{URL::to('uploadfiles/news',array($n->content_picture))}}" alt="demo1_1"></div>
  </article>
   @endforeach

</section>
  </div> 
</div><!--/.container-->

</section><!--/#feature-->

        <section id="recent-works">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2  style="color: #028749"> <a href="{{ URL::to('allgallery')}}">ผลงานของเรา</a></h2>
                <p class="lead">ผลงานที่ห้างหุ่นส่วนจำกัด ป.แจ่มสุวรรณทรัพย์ <br> ดำเนินการเสร็จเรียบร้อย</p>
            </div>
  
            <div class="slider1">
            @foreach($Gallery as $gallery =>$g)
             <div class="slide">
                <img class="slide " src="{{ URL::to('uploadfiles/gallery/thumb',$g->files_newname)}}">
                <div class="bx-caption"><span> <a href="{{ URL::to('gallery',$g->gallery_url)}}">
            {{ $g->gallery_name }}
          </a></span>  </div>
            </div>
                        
                      
            @endforeach

            </div><!--/.row-->

        </div><!--/.container-->
    </section><!--/#recent-works-->
 
     
@stop