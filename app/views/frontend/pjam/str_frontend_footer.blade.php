
       
    <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>ห้างหุ้นส่วนจำกัด</h3>
                        <h2>ป.แจ่มสุวรรณทรัพย์</h2>
                       <h2>218/13 ม.2 ต.นอกเมือง</h2>
                       <h2>อ.เมือง จ.สุรินทร์</h2>
                       <h2>32000</h2>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>ติดต่อเรา</h3>
                         <h2>เบอร์โทร 044-040-539</h2>
                         <h2>แฟกซ์ 044-530-067</h2>
                    </div>    
                </div><!--/.col-md-3-->

               

             <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>บริการของเรา</h3>
                        <ul>
                           @foreach($service as $servicess =>$servs)
                            <li><a href="{{ URL::to('submenu',$servs->s_url)}}" ><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> {{ $servs->submenu_name}}</a></li>
                            @endforeach 
                        </ul>
                    </div>    
                </div> 
   
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>แผนที่
                        </h3>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2574.7274345786773!2d103.52344769981941!3d14.876970773817499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x63664e1cf6afac3e!2z4Lir4Lih4Li54LmI4Lia4LmJ4Liy4LiZ4Liq4Li04LiZ4LiX4Lij!5e0!3m2!1sth!2sth!4v1478066008923" width="350" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>    
                </div>

            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 <a target="_blank" href="{{ URL::to('/')}}" title="">{{ $env->web_name_lo }}</a>. All Rights Reserved.
                </div>
               {{--  <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
    </footer><!--/#footer-->
 
 

   <script src="{{ URL::to('frontend/pjam/js/jquery.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/jquery.isotope.min.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/main.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/wow.min.js')}}"></script>
 
    <script src="{{ URL::to('frontend/pjam/js/slippry.min.js')}}"></script>
    <script src="{{ URL::to('frontend/pjam/js/jquery.bxslider.min.js')}}"></script>
    <script src="{{ URL::to('js/jquery.fancybox.js')}}"></script>
 <script type="text/javascript">
 $(function(){
jQuery('#news-demo').slippry({
  // general elements & wrapper
  slippryWrapper: '<div class="sy-box news-slider" />', // wrapper to wrap everything, including pager
  elements: 'article', // elments cointaining slide content

  // options
  adaptiveHeight: false, // height of the sliders adapts to current 
  captions: false,

  // pager
  pagerClass: 'news-pager',
  pager: false,
  // transitions
  transition: 'horizontal', // fade, horizontal, kenburns, false
  speed: 1200,
  pause: 3000,

  // slideshow
  autoDirection: 'prev'
});

});
 $(document).ready(function(){

    $('.carousel').carousel({
      interval: 3000 
    });
  
  $('.slider1').bxSlider({
    slideWidth: 280,
    minSlides: 2,
    maxSlides: 4,
    slideMargin: 10,
     captions: true
  });
   $(".fancybox-thumb").fancybox({
        prevEffect  : 'none',
        nextEffect  : 'none',
        helpers : {
            title   : {
                type: 'outside'
            },
            thumbs  : {
                width   : 50,
                height  : 50
            }
        }
    });
});
</script>
 @yield('script')
</body>
</html>
