
       <footer>
       
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

 
 

<script src="{{ URL::to('frontend/default/js/jquery.js')}}"></script>
<script src="{{ URL::to('frontend/default/js/bootstrap.js')}}"></script>
<script src="{{ URL::to('js/pgwslider.min.js')}}"></script>
<script src="{{ URL::to('js/jquery.fancybox.js')}}"></script>
 <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
<script>
$(document).ready(function() {
  
    $('.pgwSlider').pgwSlider();
    $(".fancybox-thumb").fancybox({
        prevEffect  : 'none',
        nextEffect  : 'none',
        helpers : {
            title   : {
                type: 'outside'
            },
            thumbs  : {
                width   : 50,
                height  : 50
            }
        }
    });
});
</script>
 
 @yield('script')
</body>
</html>