<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keyword" content="@yield('keyword')">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="{{ URL::to('frontend/default/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ URL::to('frontend/default/css/bootstrap-flat.css') }}">
    <link rel="stylesheet" href="{{ URL::to('frontend/default/css/bootstrap-responsive.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }} ">
    <link rel="stylesheet" href="{{ URL::to('frontend/default/css/styles.css') }}">
    <link rel="stylesheet" href="{{ URL::to('frontend/default/css/jquery.bxslider.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/pgwslider.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/modern-business.css') }}">


 

     

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="{{ URL::to('frontend/default/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('frontend/default/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('frontend/default/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('frontend/default/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::to('frontend/default/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head>

<body >

