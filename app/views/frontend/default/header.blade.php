    <!-- Page Content -->
{{--     <div class="box-mainweb" >
    <center>
      <div class="logo">
       <img src="{{ URL::to('img/pj_logo.jpg')}}" class="img-responsive" alt="">
       </div>
       </center> --}}
   <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     <a class="navbar-brand" href="index.html">{{ $env->web_name_lo }}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
         <li><a href="{{URL::to('/')}}">หน้าหลัก</a></li>
      @foreach($mainmenu_top as $menu =>$m)
                            @if($m->mainmenu_type=="1")
                                <li><a href="{{ URL::to('menu',array($m->m_url))}}">{{$m->mainmenu_name}}</a></li>
                                @elseif($m->mainmenu_type=="2")
                                <li><a href="{{ $m->mainmenu_url}}">{{$m->mainmenu_name}}</a></li>
                                @elseif($m->mainmenu_type=="3")
                                    <?php   $num = Mainmenu::where('parent_id',$m->id)->count(); ?>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                {{$m->mainmenu_name}} <span class="caret"></span></a>
                                     <ul class="dropdown-menu">
                                        <?php 
                                        $Submenu = Submenu::where(array('submenu_categories'=>$m->id,'submenu_show'=>'1'))->orderBy('submenu_sorting','asc')->get(); 
                                        ?>
                                        @foreach($Submenu as $submenu =>$sm)
                                        @if($sm->submenu_type=="1")
                                        <li><a href="{{ URL::to('submenu',$sm->s_url)}}" target="_blank">
                                        <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
 {{ $sm->submenu_name}}</a>
                                        </li>  
                                        @elseif($sm->submenu_type=="2")
                                        <li><a href="{{ $sm->submenu_url}}" target="_blank"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
 {{ $sm->submenu_name}}</a></li>  
                                        @endif
                                        @endforeach
                                      </ul>              
                                </li>
                            @endif
    @endforeach
        
      </ul>
       
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


 
 




