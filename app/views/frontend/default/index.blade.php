@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$env->web_name_lo)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

 <!-- Page Content -->
     <header id="myCarousel" class="carousel slide">
  <div class="carousel-inner">
    @foreach($Banner as $banner => $b)
    <div class="item @if($i==1) active @endif">
      <a href="{{ URL::to('slide',array($b->banner_url))}}" target="_blank">
       <img src="{{ URL::to('uploadfiles/banner',$b->files_newname)}}" style="width:100%;height:500px"  />
       </a>
      <div class="carousel-caption">
        <h3>{{ $b->banner_name}}</h3>
      </div>
    </div>
    <?php $i++;?>
    @endforeach
  </div>
 
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- Carousel -->
</header>
    <div class="container ">

        <!-- Marketing Icons Section -->
{{--         <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    ข่าวสารและกิจกรรมต่างๆ
                </h1>
            </div>
            @foreach($news as $new => $n)
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><a target="_blank" href="{{ URL::to('news',array($n->content_url))}}">{{ $n->content_name}}</a></h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            @endforeach
    </div> --}}
     <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">ข่าวสารและกิจกรรมต่างๆ</h2>
            </div>
            <div class="col-md-5">
            @foreach($firstnews as $fnew => $fn)
             <a target="_blank" href="{{ URL::to('news',array($fn->content_url))}}">
            <img class="img-responsive"  src="{{URL::to('uploadfiles/news/thumb',array($fn->content_picture))}}" width="500" style="height:300px">

            </a>
             <h4><a target="_blank" href="{{ URL::to('news',array($fn->content_url))}}">{{ $fn->content_name}}</a></h4>
      
              {{$fn->content_detail}}
            @endforeach
            </div>
            <div class="col-md-7">
               @foreach($news as $new => $n)
               <div class="media news-content" >
            <div class="col-xs-4" style="margin-left:-15px;"> 
            <a target="_blank" href="{{ URL::to('news',array($n->content_url))}}">
            <img class="img-responsive"  src="{{URL::to('uploadfiles/news/thumb',array($n->content_picture))}}">
            </a>
            </div>
            <div class="col-xs-8">
              <h4><a target="_blank" href="{{ URL::to('news',array($n->content_url))}}">{{ $n->content_name}}</a></h4>
      
              {{$n->content_detail}}
          
            </div>
          </div>
               @endforeach
            </div>
            
        </div>

        <!-- /.row -->

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">อัลบั้มภาพกิจกรรมต่างๆ</h2>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
                </a>
            </div>
        </div>
        <!-- /.row -->
  
        <hr>

        <!-- Call to Action Section -->
        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="#">Call to Action</a>
                </div>
            </div>
        </div>

        <hr>

    

    </div>
    <!-- /.container -->
@stop