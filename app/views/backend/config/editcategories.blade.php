@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">

                                                {{ Form::open(array(
                                                  'method'=>'POST','name'=>'form'
                                                  ,'id'=>'form-add','class'=>'form-group'
                                                  ,'url'=>'backend/edit/categories'
                                                  ), $rules)}}
                                                <div class="form-group">
                                                <label for="">{{ Lang::get('msg.categories_name', array(), 'th')}}</label>
                                                {{Form::input('text', 'txt_name',$name, 
                                                array(
                                                  'class'=>'form-control'

                                                  ))}}
                                                </div>
                                                  <div class="form-group">
                                                  <label for="">{{ Lang::get('msg.subcategories', array(), 'th')}}</label>
                                                  <select name="parent_id"  data-validetta="required" class="form-control">
                                                    <option value="0">
                                                      {{ Lang::get('msg.parent_item',array(),'th')}}
                                                    </option>
                                                    {{ Helpers::get_menu($sql)}}
                                                  </select>
                                                </div>
                                                  <br>
                                           
                                                 <div class="row">
                       <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_showhome', array(), 'th') }}</label>
                      <br>
                        <input type="radio" name="txt_showhome" value="1" @if($categories_showhome=="1") checked="true"  @endif> แสดงหน้าแรก
                        <input type="radio" name="txt_showhome" value="0"  @if($categories_showhome=="0") checked="true"  @endif> ไม่แสดงหน้าแรก
                      
                              
                    </div>
                     <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }}</label>
                       <br>
                        <input type="radio" name="txt_show" value="1"  @if($categories_show=="1") checked="true" @endif> เผยแพร่
                        <input type="radio" name="txt_show" value="0"  @if($categories_show=="0") checked="true"  @endif> ยังไม่เผยแพร่             
                              
                    </div>
                    </div>
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get('msg.msg_cancle',array(), 'th')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-circle"></i>
                                                {{ Lang::get('msg.msg_submit',array(), 'th')}}
                                            </button>
                                            {{ Form::hidden("id",$id)}}
                                                                                        {{ Form::close()}}
                                                </div>
       </section>
      </div>
@stop